" --- MY VIM CONFIG FILE ---

" --- 1. LOAD PLUGINS ---

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'chriskempson/base16-vim'
call vundle#end()
set nocompatible
filetype off
filetype plugin indent on

" --- 2. GLOBAL REMAPS ---

set number
set ttimeout
set ttimeoutlen=100

" Window navigation remaps
nmap <silent> <A-Up>    :wincmd k<CR>
nmap <silent> <A-Down>  :wincmd j<CR>
nmap <silent> <A-Left>  :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" --- 3. THEMES ---

set guifont=InputRegular:h10
set background=dark
"colorscheme base16-flat
let base16colorspace=256

" --- 4. PLUGIN-SPECIFIC CONFIGS ---

" --- 4.1. AIRLINE ---

set laststatus=2
let g:airline_theme='base16_google'
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
     let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" --- 4.2. NERDTree ---

map <F9> :NERDTreeToggle<CR>
